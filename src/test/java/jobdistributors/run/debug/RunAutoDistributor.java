package jobdistributors.run.debug;

import ij.IJ;
import ij.ImageJ;
import metaphase.timelapse.jobdistributors.JobDistributor_AutoSelectionMultipleCells_3Steps;

public class RunAutoDistributor {
	public static void main(String[] args) {
		Class<?> clazz = JobDistributor_AutoSelectionMultipleCells_3Steps.class;

		// start ImageJ
		new ImageJ();

		//IJ.runPlugIn(clazz.getName(),"Debug run");
		IJ.runPlugIn(clazz.getName(),"");
	}
}
