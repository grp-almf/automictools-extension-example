package automic.online.jobs.demo;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.online.microscope.ZeissKeys;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.ArrIndUtils;
import automic.utils.DebugVisualiserSettings;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import de.embl.cba.utils.ij.Close_all_forced;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.measure.ResultsTable;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.RankFilters;
import ij.plugin.frame.RoiManager;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

public class Job_SelectMetaphaseCells_ImageJCommand  extends Job_Default{
	
	
	public static final String KEY_COMMAND_NAME="Command name";
	public static final String KEY_X_COLUMN_NAME="X position column";
	public static final String KEY_Y_COLUMN_NAME="Y position column";
	//public static final String KEY_Z_COLUMN_NAME="Z position column";
	
	public static final String KEY_NUCLEI_MAX_COUNT="Maximal number of nuclei to select";

	
	private ImagePlus img=null;
	private Point2D.Double[] identifiedPoints;
	private Point2D.Double[] selectedPoints;
	private double selectedZ;
	private Roi[] identifiedCellRois;
	
	private String commmandName="Segment image";
	private String xColumn="X";
	private String yColumn="Y";
	//private String zColumn="Z";
	
	private int nucMaxCount;
	@Override
	protected void cleanIterOutput(){
		img=null;
		identifiedPoints=null;
		selectedPoints=null;
		identifiedCellRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		//currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		
		TimeUnit.MILLISECONDS.sleep(2000);		
		//img=ImageOpenerWithBioformats.openImage(newImgFile);
		img=openSelectedSlices(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=openSelectedSlices(newImgFile);
	}
	
	
	private ImagePlus openSelectedSlices(File _imageFile)throws Exception{
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(true);
		options.setId(_imageFile.getAbsolutePath());
		
		return BF.openImagePlus(options)[0];
	}
	@Override
	protected boolean runProcessing()throws Exception{
		this.showDebug(img, "Original image", true);
		
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		
		img.show();
		
		IJ.run(img, commmandName, "");
		
		
		img.hide();
		
		//new Close_all_forced();
		
		
		ResultsTable rt=ResultsTable.getResultsTable();
		
		if (rt==null)
			return false;
		
		if (rt.size()<1)
			return false;
		
		identifiedCellRois=rm.getRoisAsArray();
		if (identifiedCellRois.length<1)
			return false;
		
		rm.runCommand("Reset");
		identifiedPoints=new Point2D.Double[rt.size()];
		
		double x,y;
		for(int iRow=0;iRow<rt.size();iRow++) {
			x=rt.getValueAsDouble(rt.getColumnIndex(xColumn), iRow);
			y=rt.getValueAsDouble(rt.getColumnIndex(yColumn), iRow);
			identifiedPoints[iRow]=new Point2D.Double(x, y);
		}
		
		rt.reset();
		
		if (identifiedPoints.length<=nucMaxCount){
			selectedPoints=identifiedPoints;
		}
		else{
			int[] selectedIndexes=ArrIndUtils.getRandomIndexes(identifiedPoints.length, nucMaxCount);
			selectedPoints=new Point2D.Double[nucMaxCount];
			for (int i=0;i<nucMaxCount;i++)
				selectedPoints[i]=identifiedPoints[selectedIndexes[i]];
		}
		selectedZ=((double)img.getNSlices()-1.0)/2.0;
		return true;
		
	}
	
	

	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (identifiedCellRois!=null)
			for (Roi r:identifiedCellRois){
				o.add(r);
			}
		if (selectedPoints!=null)
			for (Point2D.Double p:selectedPoints) {
				o.add(new PointRoi(p.x, p.y));
			}
		
		return o;
		
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		img.setDisplayMode(IJ.COLOR);
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		
		//PointRoi saveRoi=null;
		//form  comma-separated strings with coordinates
		String xstr="", ystr="",zstr="";
		// submit 3d points in future
		for (int i=0;i<selectedPoints.length;i++){
			if (i>0){
				xstr+=";";
				ystr+=";";
				zstr+=";";
			}
			xstr+=selectedPoints[i].x;
			ystr+=selectedPoints[i].y;
			zstr+=selectedZ;
		}

		
		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr,ystr,zstr,"","","","","");
		
		saveRoisForImage(newImgFile, getOverlay().toArray());
		
		this.setSharedValue("Zoom Points", selectedPoints);
		this.setSharedValue("Zoom Counter", -1);
		this.setSharedValue("Selected Embryo Rois", identifiedCellRois);

	}
	
	@Override
	public void  postProcessFail()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("nothing","","","","","","","", "Suitable object is not found");
		this.recordSummaryDataset(false);
	}
	
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_COMMAND_NAME, null, commmandName, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_X_COLUMN_NAME, null, xColumn, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_Y_COLUMN_NAME, null, yColumn, ParameterType.STRING_PARAMETER);
		
		jobCollection.addParameter(KEY_NUCLEI_MAX_COUNT,null, 4, ParameterType.INT_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.commmandName=(String)_jobParameterCollection.getParameterValue(KEY_COMMAND_NAME);
		this.xColumn=(String)_jobParameterCollection.getParameterValue(KEY_X_COLUMN_NAME);
		this.yColumn=(String)_jobParameterCollection.getParameterValue(KEY_Y_COLUMN_NAME);
		
		this.nucMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEI_MAX_COUNT);
	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-2, 10,10,2);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		System.getProperties().setProperty("plugins.dir", "D:/Alex_work/runnung soft/Fiji.app_2019/plugins");
		
		// start ImageJ
		new ImageJ();
		
		String tblPth="D:\\Alex_work\\feedback-microscopy-workshop-data\\mitotic-cells--original-data";
		String tblFnm="experiment_summary.txt";
		Job_SelectMetaphaseCells_ImageJCommand testJob=new Job_SelectMetaphaseCells_ImageJCommand();
		testJob.initialise(null, "LZ.Image", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
