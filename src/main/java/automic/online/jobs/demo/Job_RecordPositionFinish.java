package automic.online.jobs.demo;

import java.awt.geom.Point2D;

import automic.online.jobs.Job_Default;

public class Job_RecordPositionFinish  extends Job_Default{
	
	@Override
	public void postProcessSuccess()throws Exception{
		int counter=(int)this.getSharedValue("Zoom Counter");
		counter+=1;
		this.setSharedValue("Zoom Counter", counter);
		Point2D.Double[] selectedPoints=(Point2D.Double[])this.getSharedValue("Zoom Points");
		Point2D.Double thisPoint=selectedPoints[counter];
		currentTable.setNumericValue(thisPoint.x, 0, "Zoom.X");
		currentTable.setNumericValue(thisPoint.y, 0, "Zoom.Y");
		
		
		this.recordSummaryDataset(true);
	}
}
