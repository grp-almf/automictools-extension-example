package automic.online.jobs.demo;

import java.awt.Color;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.online.microscope.ZeissKeys;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.ArrIndUtils;
import automic.utils.DebugVisualiserSettings;
import automic.utils.roi.ParticleFilterer;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.RankFilters;
import ij.plugin.frame.RoiManager;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

public class Job_SelectMetaphaseCells_Ilastik  extends Job_Default{
	
	
	public static final String KEY_SEGMENTATION_CHANNEL_INDEX="Segmentation channel";
	public static final String KEY_INTENSITY_THRESHOLD="Intensity Threshold";
	public static final String KEY_NUCLEUS_MIN_AREA="Nucleus minimal area";
	public static final String KEY_NUCLEUS_MAX_AREA="Nucleus maximal area";
	public static final String KEY_NUCLEUS_MIN_INTENSITY="Nucleus minimal intensity";
	public static final String KEY_NUCLEUS_MAX_INTENSITY="Nucleus maximal intensity";
	public static final String KEY_NUCLEUS_MIN_AR="Nucleus minimal aspect ratio";
	public static final String KEY_NUCLEUS_MAX_AR="Nucleus maximal aspect ratio";
	public static final String KEY_NUCLEUS_MIN_LONG_AXIS="Nucleus minimal long axis";
	public static final String KEY_NUCLEUS_MAX_LONG_AXIS="Nucleus maximal long axis";
	public static final String KEY_NUCLEUS_MIN_SHORT_AXIS="Nucleus minimal short axis";
	public static final String KEY_NUCLEUS_MAX_SHORT_AXIS="Nucleus maximal short axis";
	
	public static final String KEY_NUCLEI_MAX_COUNT="Maximal number of nuclei to select";

	
	private ImagePlus img=null;
	private Point2D.Double[] selectedPoints;
	private double selectedZ;
	private Roi[] identifiedCellRois;
	private Roi[] filteredCellRois;
	private Roi[] selectedCellRois;
	
	private int segChannel;
	private int segThreshold;
	
	private int nucMinSize;
	private int nucMaxSize;
	private double nucMinIntensity;
	private double nucMaxIntensity;
	private double nucMinAR;
	private double nucMaxAR;
	private double nucMinLongAxis;
	private double nucMaxLongAxis;
	private double nucMinShortAxis;
	private double nucMaxShortAxis;

	
	
	private int nucMaxCount;
	@Override
	protected void cleanIterOutput(){
		img=null;
		selectedPoints=null;
		identifiedCellRois=null;
		filteredCellRois=null;
		selectedCellRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//super.clearSharedData();
		//currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		
		TimeUnit.MILLISECONDS.sleep(2000);		
		//img=ImageOpenerWithBioformats.openImage(newImgFile);
		img=openSelectedSlices(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=openSelectedSlices(newImgFile);
	}
	
	
	private ImagePlus openSelectedSlices(File _imageFile)throws Exception{
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(true);
		options.setId(_imageFile.getAbsolutePath());
		
		return BF.openImagePlus(options)[0];
	}
	@Override
	protected boolean runProcessing()throws Exception{
		this.showDebug(img, "original image", true);

		ImagePlus segStackImage=new Duplicator().run(img, segChannel, segChannel, 1, img.getNSlices(), 1, 1);
		selectedZ=(img.getNSlices()+1)/2.0;
		ZProjector maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		maxProjector.setImage(segStackImage);
		maxProjector.doProjection();
		ImagePlus maxProjectedImage=maxProjector.getProjection();
		
		segmentNuclei(maxProjectedImage);
		
		if (filteredCellRois==null) return false;
		if (filteredCellRois.length<1)	return false;

		if (filteredCellRois.length<=nucMaxCount){
			selectedCellRois=filteredCellRois;
		}
		else{
			int[] selectedIndexes=ArrIndUtils.getRandomIndexes(filteredCellRois.length, nucMaxCount);
			selectedCellRois=new Roi[nucMaxCount];
			for (int i=0;i<nucMaxCount;i++)
				selectedCellRois[i]=filteredCellRois[selectedIndexes[i]];
		}
		
		for (Roi r:selectedCellRois)
			r.setStrokeColor(Color.cyan);

		
		
		double x,y;
		selectedPoints=new Point2D.Double[selectedCellRois.length];
		for (int i=0;i<selectedCellRois.length;i++){
			x=selectedCellRois[i].getContourCentroid()[0];
			y=selectedCellRois[i].getContourCentroid()[1];
			selectedPoints[i]=new Point2D.Double(x, y);
		}
		
		
		return true;
		
	}
	
	
	private void segmentNuclei(ImagePlus _embryoImage)throws Exception {
		new RankFilters().rank(_embryoImage.getProcessor(), 3, RankFilters.MEDIAN);
		this.showDebug(_embryoImage, "Image for embryo segmentation", true);
		IJ.setThreshold(_embryoImage, segThreshold, Double.MAX_VALUE);
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		ParticleAnalyzer pAnalyzer=new ParticleAnalyzer(ParticleAnalyzer.ADD_TO_MANAGER|ParticleAnalyzer.SHOW_NONE|ParticleAnalyzer.FOUR_CONNECTED|ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES, 
														0,
														null,
														nucMinSize, nucMaxSize, 0.0, 1.0);
		
		pAnalyzer.analyze(_embryoImage);
		if (rm.getCount()<1)
			return;

		identifiedCellRois=rm.getRoisAsArray();
		for (Roi r:identifiedCellRois)
			r.setStrokeColor(Color.red);

		
		ParticleFilterer nucleiFilter=new ParticleFilterer(_embryoImage.getProcessor(), identifiedCellRois);
		nucleiFilter.filterThr(ParticleFilterer.MEAN, nucMinIntensity, nucMaxIntensity);
		nucleiFilter.filterThr(ParticleFilterer.ASPECT_RATIO, nucMinAR, nucMaxAR);
		nucleiFilter.filterThr(ParticleFilterer.LONG_AXIS, nucMinLongAxis, nucMaxLongAxis);
		nucleiFilter.filterThr(ParticleFilterer.SHORT_AXIS, nucMinShortAxis, nucMaxShortAxis);
		filteredCellRois=nucleiFilter.getPassedRois();
		
		if (filteredCellRois==null)
			return;
		for (Roi r:filteredCellRois)
			r.setStrokeColor(Color.yellow);
		
		return;
	}
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (identifiedCellRois!=null)
			for (Roi r:identifiedCellRois){
				o.add(r);
			}
		
		return o;
		
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		img.setDisplayMode(IJ.COMPOSITE);
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
		img.setC(segChannel);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		
		//PointRoi saveRoi=null;
		//form  comma-separated strings with coordinates
		String xstr="", ystr="",zstr="";
		// submit 3d points in future
		for (int i=0;i<selectedCellRois.length;i++){
			if (i>0){
				xstr+=";";
				ystr+=";";
				zstr+=";";
			}
			xstr+=selectedPoints[i].x;
			ystr+=selectedPoints[i].y;
			zstr+=selectedZ;
		}

		
		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr,ystr,zstr,"","","","","");
		
		saveRoisForImage(newImgFile, getOverlay().toArray());
		//saveRoiForImage(newImgFile,saveRoi);
		
		//this.setSharedValue("Zoom Points", selectedPoints);
		//this.setSharedValue("Zoom Counter", -1);
		//this.setSharedValue("Selected Embryo Rois", selectedCellRois);
		
		//this.recordSummaryDataset(true);
	}
	
	@Override
	public void  postProcessFail()throws Exception{
		this.recordSummaryDataset(false);
	}
	
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_SEGMENTATION_CHANNEL_INDEX, null, 1, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_INTENSITY_THRESHOLD, null, 25, ParameterType.INT_PARAMETER);

		jobCollection.addParameter(KEY_NUCLEUS_MIN_AREA, null, 500, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_AREA, null, 1500, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_INTENSITY, null, 50.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_INTENSITY, null, 250.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_AR, null, 2.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_AR, null, 20.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_LONG_AXIS, null, 30.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_LONG_AXIS, null, 250.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MIN_SHORT_AXIS, null, 10.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEUS_MAX_SHORT_AXIS, null, 20.0, ParameterType.DOUBLE_PARAMETER);
		
		jobCollection.addParameter(KEY_NUCLEI_MAX_COUNT,null, 4, ParameterType.INT_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.segChannel=(Integer)_jobParameterCollection.getParameterValue(KEY_SEGMENTATION_CHANNEL_INDEX);
		this.segThreshold=(Integer)_jobParameterCollection.getParameterValue(KEY_INTENSITY_THRESHOLD);
		
		this.nucMinSize=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_AREA);
		this.nucMaxSize=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_AREA);
		this.nucMinIntensity=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_INTENSITY);
		this.nucMaxIntensity=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_INTENSITY);
		this.nucMinAR=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_AR);
		this.nucMaxAR=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_AR);
		this.nucMinLongAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_LONG_AXIS);
		this.nucMaxLongAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_LONG_AXIS);
		this.nucMinShortAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MIN_SHORT_AXIS);
		this.nucMaxShortAxis=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEUS_MAX_SHORT_AXIS);
		
		
		
		this.nucMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEI_MAX_COUNT);
	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-2, 10,10,2);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="C:/tempDat/Sara_Cuylen/02_raw_data/63x_zoom0c8_4Z";
		String tblFnm="test_table.txt";
		Job_SelectMetaphaseCells_Ilastik testJob=new Job_SelectMetaphaseCells_Ilastik();
		testJob.initialise(null, "LZ.Image", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
