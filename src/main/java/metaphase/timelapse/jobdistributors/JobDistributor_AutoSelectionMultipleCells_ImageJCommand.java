package metaphase.timelapse.jobdistributors;


//import ij.Prefs;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.common.Job_AutofocusInit;
import automic.online.jobs.demo.Job_RecordPositionFinish;
import automic.online.jobs.demo.Job_SelectMetaphaseCells_ImageJCommand;
import automic.table.TableModel;
import automic.table.TableProcessor;


public class JobDistributor_AutoSelectionMultipleCells_ImageJCommand extends JobDistributor_Abstract implements PlugIn {
	
	//private static final String PREFS_PREFIX="feebback.fly.embryo";
	//private ParameterCollection manualTrackParameterCollectionSelect;
	//private ParameterCollection manualTrackParameterCollectionFocus;

	
	@Override
	protected void fillJobList(){
		super.addImageJob(Job_AutofocusInit.class,	"AF",  "AFocus",	true);
		super.addImageJob(Job_SelectMetaphaseCells_ImageJCommand.class,	"LowZoom--",  "LZ.Image",	true);
		super.addImageJob(Job_RecordPositionFinish.class,	"HighZoom--",  "HZ.Image",	true);
	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");

		outTbl.addFileColumns("AFocus", "IMG");
		outTbl.addFileColumns("LZ.Image", "IMG");
		outTbl.addFileColumns("HZ.Image", "IMG");

		outTbl.addValueColumn("Zoom.X", "NUM");
		outTbl.addValueColumn("Zoom.Y", "NUM");
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		TableProcessor tProcessor=new TableProcessor(_tModel);
		return tProcessor;
	}

	
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
		//manualTrackParameterCollectionSelect=new Job_SelectMultipleEmbryosAuto().createJobParameters();
		//manualTrackParameterCollectionFocus=new Job_FocusEmbryoAuto().createJobParameters();
		
		//_dialog.addCheckbox(Job_SelectEmbryoManual.KEY_INVERT_Z, Prefs.getBoolean(generatePrefsKey(Job_SelectEmbryoManual.KEY_INVERT_Z),false));
		//_dialog.addNumericField(Job_SelectEmbryoManual.KEY_LOCATION_X, Prefs.getInt(generatePrefsKey(Job_SelectEmbryoManual.KEY_LOCATION_X),1000), 0);
		//_dialog.addNumericField(Job_SelectEmbryoManual.KEY_LOCATION_Y, Prefs.getInt(generatePrefsKey(Job_SelectEmbryoManual.KEY_LOCATION_Y),120), 0);
		//_dialog.addNumericField(Job_SelectEmbryoManual.KEY_ZOOM_ITERATIONS, Prefs.getInt(generatePrefsKey(Job_SelectEmbryoManual.KEY_ZOOM_ITERATIONS),2), 0);
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
		//manualTrackParameterCollectionSelect.setUndefinedValuesFromDefaults();
		//manualTrackParameterCollectionFocus.setUndefinedValuesFromDefaults();
//		boolean invertZ=_dialog.getNextBoolean();
//		manualTrackParameterCollection.setParameterValue(Job_SelectEmbryoManual.KEY_INVERT_Z, invertZ);
//		Prefs.set(generatePrefsKey(Job_SelectEmbryoManual.KEY_INVERT_Z), invertZ);
//		
//		int locationX=(int)_dialog.getNextNumber();
//		manualTrackParameterCollection.setParameterValue(Job_SelectEmbryoManual.KEY_LOCATION_X, locationX);
//		Prefs.set(generatePrefsKey(Job_SelectEmbryoManual.KEY_LOCATION_X), invertZ);
//		
//		int locationY=(int)_dialog.getNextNumber();
//		manualTrackParameterCollection.setParameterValue(Job_SelectEmbryoManual.KEY_LOCATION_Y, locationY);
//		Prefs.set(generatePrefsKey(Job_SelectEmbryoManual.KEY_LOCATION_Y), invertZ);
//		
//		int zoomIterations=(int)_dialog.getNextNumber();
//		manualTrackParameterCollection.setParameterValue(Job_SelectEmbryoManual.KEY_ZOOM_ITERATIONS, zoomIterations);
//		Prefs.set(generatePrefsKey(Job_SelectEmbryoManual.KEY_ZOOM_ITERATIONS), invertZ);
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}


	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="C:/tempDat/AutoFRAP_test";
		this.setGeneralOptions(searchPath, true, false);
		//this.fileExtension="lsm";
		
		//manualTrackParameterCollectionSelect=new Job_SelectMultipleEmbryosAuto().createJobParameters();
		//manualTrackParameterCollectionSelect.setUndefinedValuesFromDefaults();
		//manualTrackParameterCollectionFocus=new Job_SelectMultipleEmbryosAuto().createJobParameters();
		//manualTrackParameterCollectionFocus.setUndefinedValuesFromDefaults();
		
	}
	
//	private String generatePrefsKey(String _key){
//		return String.format("%s.%s",PREFS_PREFIX, _key);
//	}
}
