package metaphase.timelapse.jobdistributors;


//import ij.Prefs;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.common.Job_AutofocusInit;
import automic.online.jobs.common.Job_AutofocusInit_ZenBlue;
import automic.online.jobs.common.Job_RecordFinish;
import automic.online.jobs.demo.Job_RecordPositionFinish;
import automic.online.jobs.demo.Job_SelectMetaphaseCells_Ilastik;
import automic.table.TableModel;
import automic.table.TableProcessor;


public class JobDistributor_AFocus extends JobDistributor_Abstract implements PlugIn {
	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_ZenBlue.class,	"AF",  "AFocus",	true);
		super.addImageJob(Job_RecordFinish.class,	"LowZoom--",  "LZ.Image",	true);

	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");

		outTbl.addFileColumns("AFocus", "IMG");
		outTbl.addFileColumns("LZ.Image", "IMG");
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		TableProcessor tProcessor=new TableProcessor(_tModel);
		return tProcessor;
	}

	
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}

	@Override
	protected void setDebugConfiguration(){
		final String searchPath="C:/tempDat/AutoFRAP_test";
		this.setGeneralOptions(searchPath, true, false);
		//this.fileExtension="lsm";
		
	}
}
