# Experiment simulation tutolial


## Prerequisites
Please complete these steps before the practical

1. Install Fiji on your computer if you did not do it before: [https://fiji.sc/](https://fiji.sc/)

2. Install Required Fiji libraries from the corresponding update sites:
	1. [Help › Update…]
	2. Press [Manage Update Sites] button
	3. Select checkboxes on front of the following items
		1. EMBL-CBA
		2. ImageScience
		3. [https://sites.imagej.net/AutoMicTools/](https://sites.imagej.net/AutoMicTools/) (for this you need to press [Add Update site] and in the line appeared on the bottom of the table type ***AutoMicTools*** in the **Name** column and ***https://sites.imagej.net/AutoMicTools/*** in the **URL** column
	3. Press [Apply changes] to install the latest versions of all plugins
	4. Restart Fiji.
3. Download example data from [here](https://oc.embl.de/index.php/s/cpdeO9IDpw355Af).  



## Workflow

1. Start Fiji
2. Use the *mitotic-cells--for-simulations*
3. Create an empty “experiment” folder somewhere on your hard drive
	1. Better do not use desktop or any other “protected” location
2. Run the automated protocol for online image analysis in frrdback microscopy experimen
	1. [Plugins › Auto Mic Tools › Common Online › AF LowZoom HighZoom - Threshold segmentation]
	2. Select the follwing parameters in the first opened dialog
		1. ***Experiment folder*** - path “experiment” folder you have just created.
		2. ***Image file extensiton*** - czi
		3. Use default values for other parameters in this dialog.
	3. Accept default parameters for image analysis Job in the second dialog
	4. Copy files one-by-one from *mitotic-cells--for-simulations* folder to “experiment” folder
	5. Stop monitoring at the end of experiment

5. Browse the results with AutoMic Browser
