# Project description

Automated Feedback Procedures for the Fly Embryo Screen.

# Experiment setup

Multichamber wide slide with fixed fly embryos stained with 3 dyes:
- DAPI: nuclei
- Alexa488: antibody binds to Ds-red
- Alexa633: antibody for LacZ.

# Feedback microscopy protocol:
1. Select manually positions corresponding to the centers of the wells
2. Tile scan overview image with 5x objective.
3. Finding positions of embryos via feedback analysis
4. ...

# Log/updates

2018-01-05 Tim and Alex
- Configuring acquisition jobs manually.
- Trying feedback experiments with manual selection procedures
- X-Y coordinates are flipping, microscope additionally scans from right to left